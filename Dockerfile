FROM golang:1.9

ENV DEP_VERSION v0.3.1

ENV GOBIN /go/bin

COPY . /go/src/github.com/karixtech/go-whatsapp/

WORKDIR /go/src/github.com/karixtech/go-whatsapp/whatsapp

RUN go get -u github.com/jstemmer/go-junit-report

