package whatsapp

import (
	"context"
	"fmt"
	"net/http"
)

type UserService service

type Users struct {
	User []struct {
		Token        string `json:"token,omitempty"`
		ExpiresAfter string `json:"expires_after,omitempty"`
	} `json:"users,omitempty"`
}

func (u *UserService) Login(ctx context.Context, username, password string) (*Users, *http.Response, error) {
	url := fmt.Sprintf("v1/users/login")

	//TODO:  Create a fresh client so roundtripper does not get used

	req, err := u.client.NewRequest("POST", url, nil)

	if err != nil {
		return nil, nil, err
	}

	req.SetBasicAuth(username, password)

	users := &Users{}

	resp, err := u.client.Do(ctx, req, users)

	if err != nil {
		return nil, nil, err
	}

	return users, resp, nil

}

func (u *UserService) Logout(ctx context.Context) (*http.Response, error) {
	url := fmt.Sprintf("v1/users/logout")

	req, err := u.client.NewRequest("POST", url, nil)

	if err != nil {
		return nil, err
	}

	resp, err := u.client.Do(ctx, req, nil)

	if err != nil {
		return nil, err
	}

	return resp, nil
}
