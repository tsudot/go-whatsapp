package whatsapp

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"reflect"
	"strings"
	"testing"
)

const (
	baseURL = "https://wa.test/"
)

func setup() (client *Client, mux *http.ServeMux, serverURL string, teardown func()) {

	mux = http.NewServeMux()

	apiHandler := http.NewServeMux()

	apiHandler.Handle("/", mux)

	// server is a test HTTP server used to provide mock API responses.
	server := httptest.NewServer(apiHandler)

	// client is the Whatsapp client being tested and is
	// configured to use test server.
	url, _ := url.Parse(server.URL + "/")
	client = NewClient(url.String(), nil)

	return client, mux, server.URL, server.Close

}

func testMethod(t *testing.T, r *http.Request, want string) {
	if got := r.Method; got != want {
		t.Errorf("Request method: %v, want %v", got, want)
	}
}

func TestNewClient(t *testing.T) {
	c := NewClient(baseURL, nil)

	if got, want := c.UserAgent, userAgent; got != want {
		t.Errorf("NewClient UserAgent is %v, want %v", got, want)
	}
}

func TestNewMediaUploadRequest(t *testing.T) {
	c := NewClient(baseURL, nil)
	inURL, outURL := "/foo", baseURL+"foo"

	var r io.Reader
	r = strings.NewReader("Read will return these bytes")
	contentLength := int64(10)
	contentType := "image/png"

	req, _ := c.NewMediaUploadRequest("POST", inURL, r, contentLength, contentType)

	if got, want := req.URL.String(), outURL; got != want {
		t.Errorf("NewMediaUploadRequest(%q) URL is %v, want %v", inURL, got, want)
	}

	// test that default user-agent is attached to the request
	if got, want := req.Header.Get("User-Agent"), c.UserAgent; got != want {
		t.Errorf("NewMediaUploadRequest() User-Agent is %v, want %v", got, want)
	}

	if got, want := req.Header.Get("Content-Type"), contentType; got != want {
		t.Errorf("NewMediaUploadRequest() Content-Type is %v, want %v", got, want)
	}

	if got, want := req.ContentLength, contentLength; got != want {
		t.Errorf("NewMediaUploadRequest() Content-Length is %v, want %v", got, want)
	}
}

func TestNewRequest(t *testing.T) {
	c := NewClient(baseURL, nil)

	inURL, outURL := "/foo", baseURL+"foo"

	inBody, outBody := &ContactRequest{Blocking: string("wait"), Contacts: []string{"+12344321"}}, `{"blocking":"wait","contacts":["+12344321"]}`+"\n"

	req, _ := c.NewRequest("GET", inURL, inBody)

	if got, want := req.URL.String(), outURL; got != want {
		t.Errorf("NewRequest(%q) URL is %v, want %v", inURL, got, want)
	}

	// test that body was JSON encoded
	body, _ := ioutil.ReadAll(req.Body)
	if got, want := string(body), outBody; got != want {
		t.Errorf("NewRequest(%q) Body is %v, want %v", inBody, got, want)
	}

	// test that default user-agent is attached to the request
	if got, want := req.Header.Get("User-Agent"), c.UserAgent; got != want {
		t.Errorf("NewRequest() User-Agent is %v, want %v", got, want)
	}
}

func TestNewRequest_invalidJSON(t *testing.T) {
	c := NewClient(baseURL, nil)

	type T struct {
		A map[interface{}]interface{}
	}
	_, err := c.NewRequest("GET", ".", &T{})

	if err == nil {
		t.Error("Expected error to be returned.")
	}
	if err, ok := err.(*json.UnsupportedTypeError); !ok {
		t.Errorf("Expected a JSON error; got %#v.", err)
	}
}

func TestDo(t *testing.T) {
	client, mux, _, teardown := setup()
	defer teardown()

	type foo struct {
		A string
	}

	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		testMethod(t, r, "GET")
		fmt.Fprint(w, `{"A":"a"}`)
	})

	req, _ := client.NewRequest("GET", ".", nil)
	body := new(foo)
	client.Do(context.Background(), req, body)

	want := &foo{"a"}
	if !reflect.DeepEqual(body, want) {
		t.Errorf("Response body = %v, want %v", body, want)
	}
}

func TestDo_httpError(t *testing.T) {
	client, mux, _, teardown := setup()
	defer teardown()

	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.Error(w, "Bad Request", 400)
	})

	req, _ := client.NewRequest("GET", ".", nil)
	resp, err := client.Do(context.Background(), req, nil)

	if err == nil {
		t.Fatal("Expected HTTP 400 error, got no error.")
	}
	if resp.StatusCode != 400 {
		t.Errorf("Expected HTTP 400 error, got %d status code.", resp.StatusCode)
	}
}

func TestAuthTransport(t *testing.T) {
	client, mux, _, teardown := setup()
	defer teardown()

	username, password := "u", "p"

	mux.HandleFunc("/v1/users/login", func(w http.ResponseWriter, r *http.Request) {
		u, p, ok := r.BasicAuth()
		if !ok {
			t.Errorf("request does not contain basic auth credentials")
		}
		if u != username {
			t.Errorf("request contained basic auth username %q, want %q", u, username)
		}
		if p != password {
			t.Errorf("request contained basic auth password %q, want %q", p, password)
		}

		fmt.Fprint(w, `{
					   "users": [{
						 "token": "eyJhbGciOHlXVCJ9.eyJ1c2VyIjoNTIzMDE2Nn0.mEoF0COaO00Z1cANo",
					     "expires_after": "2045-03-01 15:29:26+00:00"
					    }]
					 }`)
	})

	tp := AuthTransport{
		Username: username,
		Password: password,
	}

	authClient := NewClient(client.BaseURL.String(), tp.Client())

	req, _ := authClient.NewRequest("GET", ".", nil)

	authClient.Do(context.Background(), req, nil)
}

func TestBasicAuthTransport_transport(t *testing.T) {
	// default transport
	tp := &AuthTransport{}
	if tp.transport() != http.DefaultTransport {
		t.Errorf("Expected http.DefaultTransport to be used.")
	}

	// custom transport
	tp = &AuthTransport{
		Transport: &http.Transport{},
	}
	if tp.transport() == http.DefaultTransport {
		t.Errorf("Expected custom transport to be used.")
	}
}
