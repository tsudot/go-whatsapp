package whatsapp

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"mime"
	"net/http"
	"strings"
)

const (
	MaxMediaLength = 64000000
	Doc            = "document"
	Image          = "image"
	Audio          = "audio"
	Video          = "video"
)

var ValidMimeSupported = map[string]string{
	"application/pdf":    Doc,
	"application/msword": Doc,
	"application/vnd.openxmlformats-officedocument.wordprocessingml.document":   Doc,
	"application/vnd.ms-powerpoint":                                             Doc,
	"application/vnd.openxmlformats-officedocument.presentationml.presentation": Doc,
	"application/vnd.ms-excel":                                                  Doc,
	"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":         Doc,
	"image/jpeg": Image,
	"image/jpg":  Image,
	"image/png":  Image,
	"audio/amr":  Audio,
	"audio/mpeg": Audio,
	"audio/aac":  Audio,
	"audio/m4a":  Audio,
	"audio/ogg":  Audio,
	"video/mp4":  Video,
	"video/3gpp": Video,
}

var WhatsappMediaType = map[string]string{
	"application/pdf":    "PDF",
	"application/msword": "DOC",
	"application/vnd.openxmlformats-officedocument.wordprocessingml.document":   "DOCX",
	"application/vnd.ms-powerpoint":                                             "PPT",
	"application/vnd.openxmlformats-officedocument.presentationml.presentation": "PPTX",
	"application/vnd.ms-excel":                                                  "XLS",
	"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":         "XLSX",
	"audio/amr":              "AMR",
	"audio/mpeg":             "MP3",
	"audio/mp4":              "M4A",
	"audio/ac3":              "AC3",
	"audio/vnd.wave":         "WAV",
	"audio/aac":              "AAC",
	"audio/ogg":              "OGG",
	"audio/ogg; codecs=opus": "OGG",
	"audio/basic":            "AU",
	"image/gif":              "GIF",
	"image/bmp":              "BMP",
	"image/jpeg":             "JPEG",
	"image/png":              "PNG",
	"video/mpeg":             "MPG",
	"video/mp4":              "MP4",
	"video/quicktime":        "MOV",
	"video/webm":             "WEBM",
	"video/3gpp":             "3GP",
	"video/3gpp2":            "3G2",
	"video/x-matroska":       "MKV",
	"video/x-m4v":            "M4V",
	"video/MP2T":             "MTS",
	"video/x-ms-vob":         "VOB",
	"video/avi":              "AVI",
	"video/x-ms-wmv":         "WMV",
	"video/x-flv":            "FLV",
}

type MediaService service

type Media struct {
	Media []struct {
		ID string `json:"id,omitempty"`
	} `json:"media,omitempty"`
}

type ClassifiedMedia struct {
	MediaData     *Media
	MediaMimeType string
	MediaClass    string
	MediaType     string
}

type DownloadedMedia struct {
	MediaID            string `json:"media_id"`
	MediaContent       io.Reader
	MediaContentLength int64
}

func (m *MediaService) UploadURL(ctx context.Context, mediaURL string) (*ClassifiedMedia, *http.Response, error) {
	url := fmt.Sprintf("v1/media")

	mresp, err := http.Get(mediaURL)

	if err != nil {
		return nil, nil, err
	}

	contentLength := mresp.ContentLength
	if len(mresp.Header["Content-Type"]) == 0 {
		return nil, nil, ErrMediaContentTypeNotSet
	}

	contentType := strings.ToLower(mresp.Header["Content-Type"][0])

	if contentLength < 0 {
		return nil, nil, ErrMediaContentLength
	}

	if contentLength > MaxMediaLength {
		// return error
		return nil, nil, ErrMediaSizeExceeded
	}

	// Read no more than 64MB
	buf := make([]byte, contentLength)

	if _, err := io.ReadFull(mresp.Body, buf); err != nil {
		return nil, nil, ErrMediaNotReadable
	}

	rmedia := bytes.NewReader(buf)

	mType, _, err := mime.ParseMediaType(contentType)

	if err != nil {
		return nil, nil, ErrMediaNotSupported
	}

	// Classify dMime into audio, document or image
	if mediaClass, ok := ValidMimeSupported[mType]; ok {

		req, err := m.client.NewMediaUploadRequest("POST", url, rmedia, contentLength, mType)

		if err != nil {
			return nil, nil, err
		}

		media := &Media{}

		resp, err := m.client.Do(ctx, req, media)

		if err != nil {
			return nil, nil, err
		}

		cMedia := &ClassifiedMedia{
			MediaData:     media,
			MediaClass:    mediaClass,
			MediaType:     ValidMimeSupported[mType],
			MediaMimeType: mType,
		}

		return cMedia, resp, nil

	}

	return nil, nil, ErrMediaNotSupported
}

// TODO: Uploading a raw file does not work at the moment
// To be implemented
func (m *MediaService) Upload(ctx context.Context, mr io.Reader, size int64, mediaType string) (*Media, *http.Response, error) {
	url := fmt.Sprintf("v1/media")

	req, err := m.client.NewMediaUploadRequest("POST", url, mr, size, mediaType)

	if err != nil {
		return nil, nil, err
	}

	media := &Media{}

	resp, err := m.client.Do(ctx, req, media)

	if err != nil {
		return nil, nil, err
	}

	return media, resp, nil

}

func (m *MediaService) Download(ctx context.Context, mediaID string) (*DownloadedMedia, *http.Response, error) {
	url := fmt.Sprintf("v1/media/" + mediaID)

	req, err := m.client.NewMediaDownloadRequest("GET", url, mediaID)

	if err != nil {
		return nil, nil, err
	}

	resp, err := m.client.Do(ctx, req, nil)

	if err != nil {
		return nil, nil, err
	}

	buf := make([]byte, resp.ContentLength)

	if _, err := io.ReadFull(resp.Body, buf); err != nil {
		return nil, nil, err
	}

	rmedia := bytes.NewReader(buf)

	media := &DownloadedMedia{}

	media.MediaContent = rmedia
	media.MediaID = mediaID
	media.MediaContentLength = resp.ContentLength

	return media, resp, nil
}
