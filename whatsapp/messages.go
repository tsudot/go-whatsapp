package whatsapp

import (
	"context"
	"fmt"
	"net/http"
)

type MessageService service

type Messages struct {
	Messages []struct {
		ID string `json:"id,omitempty"`
	} `json:"messages,omitempty"`
	Errors []Error `json:"errors,omitempty"`
}

type MessageBase struct {
	RecipientType string `json:"recipient_type,omitempty"`
	To            string `json:"to,omitempty"`
	MessageType   string `json:"type,omitempty"`
}

type TextMessage struct {
	MessageBase
	PreviewURL bool `json:"preview_url,omitempty"`
	Text       struct {
		Body string `json:"body,omitempty"`
	} `json:"text,omitempty"`
}

type HSMTextMessage struct {
	MessageBase
	HSM struct {
		Namespace   string `json:"namespace,omitempty"`
		ElementName string `json:"element_name,omitempty"`

		Language struct {
			Policy string `json:"policy,omitempty"`
			Code   string `json:"code,omitempty"`
		} `json:"language,omitempty"`

		LocalizableParams []struct {
			Default string `json:"default,omitempty"`

			Currency *struct {
				CurrencyCode string `json:"currency_code,omitempty"`
				Amount       string `json:"amount_1000,omitempty"`
			} `json:"currency,omitempty"`

			DateTime *struct {
				Component struct {
					DayOfWeek  string `json:"day_of_week,omitempty"`
					DayOfMonth string `json:"day_of_month,omitempty"`
					Year       string `json:"year,omitempty"`
					Month      string `json:"month,omitempty"`
					Hour       string `json:"hour,omitempty"`
					Minute     string `json:"minute,omitempty"`
				} `json:"component,omitempty"`
			} `json:"date_time,omitempty"`
		} `json:"localizable_params,omitempty"`
	} `json:"hsm,omitempty"`

	TTL string `json:"ttl,omitempty"`
}

type MediaMeta struct {
	ID      string `json:"id,omitempty"`
	Caption string `json:"caption,omitempty"`
}

type AudioMediaMessage struct {
	MessageBase
	Audio *MediaMeta `json:"audio,omitempty"`
}

type VideoMediaMessage struct {
	MessageBase
	Video *MediaMeta `json:"video,omitempty"`
}

type DocumentMediaMessage struct {
	MessageBase
	Document *MediaMeta `json:"document,omitempty"`
}

type ImageMediaMessage struct {
	MessageBase
	Image *MediaMeta `json:"image,omitempty"`
}

type LocationTextMessage struct {
	MessageBase
	Location struct {
		Longitude string `json:"longitude,omitempty"`
		Latitude  string `json:"latitude,omitempty"`
		Name      string `json:"name,omitempty"`
		Address   string `json:"address,omitempty"`
	} `json:"location,omitempty"`
}

func (m *MessageService) SendDocumentMedia(ctx context.Context, mr *DocumentMediaMessage) (*Messages, *http.Response, error) {
	url := fmt.Sprintf("v1/messages")

	req, err := m.client.NewRequest("POST", url, mr)

	if err != nil {
		return nil, nil, err
	}

	messages := &Messages{}

	resp, err := m.client.Do(ctx, req, messages)

	if err != nil {
		return nil, nil, err
	}

	return messages, resp, nil

}

func (m *MessageService) SendAudioMedia(ctx context.Context, mr *AudioMediaMessage) (*Messages, *http.Response, error) {
	url := fmt.Sprintf("v1/messages")

	req, err := m.client.NewRequest("POST", url, mr)

	if err != nil {
		return nil, nil, err
	}

	messages := &Messages{}

	resp, err := m.client.Do(ctx, req, messages)

	if err != nil {
		return nil, nil, err
	}

	return messages, resp, nil

}

func (m *MessageService) SendVideoMedia(ctx context.Context, mr *VideoMediaMessage) (*Messages, *http.Response, error) {
	url := fmt.Sprintf("v1/messages")

	req, err := m.client.NewRequest("POST", url, mr)

	if err != nil {
		return nil, nil, err
	}

	messages := &Messages{}

	resp, err := m.client.Do(ctx, req, messages)

	if err != nil {
		return nil, nil, err
	}

	return messages, resp, nil

}

func (m *MessageService) SendImageMedia(ctx context.Context, mr *ImageMediaMessage) (*Messages, *http.Response, error) {
	url := fmt.Sprintf("v1/messages")

	req, err := m.client.NewRequest("POST", url, mr)

	if err != nil {
		return nil, nil, err
	}

	messages := &Messages{}

	resp, err := m.client.Do(ctx, req, messages)

	if err != nil {
		return nil, nil, err
	}

	return messages, resp, nil

}

func (m *MessageService) SendLocation(ctx context.Context, mr *LocationTextMessage) (*Messages, *http.Response, error) {
	url := fmt.Sprintf("v1/messages")

	req, err := m.client.NewRequest("POST", url, mr)

	if err != nil {
		return nil, nil, err
	}

	messages := &Messages{}

	resp, err := m.client.Do(ctx, req, messages)

	if err != nil {
		return nil, nil, err
	}

	return messages, resp, nil

}

func (m *MessageService) SendHSM(ctx context.Context, mr *HSMTextMessage) (*Messages, *http.Response, error) {
	url := fmt.Sprintf("v1/messages")

	req, err := m.client.NewRequest("POST", url, mr)

	if err != nil {
		return nil, nil, err
	}

	messages := &Messages{}

	resp, err := m.client.Do(ctx, req, messages)

	if err != nil {
		return nil, nil, err
	}

	return messages, resp, nil
}

func (m *MessageService) SendText(ctx context.Context, mr *TextMessage) (*Messages, *http.Response, error) {
	url := fmt.Sprintf("v1/messages")

	req, err := m.client.NewRequest("POST", url, mr)

	if err != nil {
		return nil, nil, err
	}

	messages := &Messages{}

	resp, err := m.client.Do(ctx, req, messages)

	if err != nil {
		return nil, nil, err
	}

	return messages, resp, nil
}
