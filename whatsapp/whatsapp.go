package whatsapp

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"sync"
	"time"
)

const (
	userAgent         = "go-whatsapp"
	expiresTimeLayout = "2006-01-02 15:04:05Z07:00"
)

type Client struct {
	client *http.Client // HTTP client used to communicate with the API.

	BaseURL *url.URL

	UserAgent string

	common service

	// Services used to talk to different parts of the Whatsapp API
	Contacts *ContactService
	Users    *UserService
	Messages *MessageService
	Media    *MediaService
}

func (c *Client) NewMediaDownloadRequest(method, urlStr, mediaID string) (*http.Request, error) {
	if !strings.HasSuffix(c.BaseURL.Path, "/") {
		return nil, fmt.Errorf("BaseURL must have a trailing slash, but %q does not", c.BaseURL)
	}

	u, err := c.BaseURL.Parse(urlStr)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(method, u.String(), nil)

	if err != nil {
		return nil, err
	}

	if c.UserAgent != "" {
		req.Header.Set("User-Agent", c.UserAgent)
	}
	return req, nil

}

func (c *Client) NewMediaUploadRequest(method, urlStr string, reader io.Reader, size int64, mediaType string) (*http.Request, error) {
	if !strings.HasSuffix(c.BaseURL.Path, "/") {
		return nil, fmt.Errorf("BaseURL must have a trailing slash, but %q does not", c.BaseURL)
	}

	u, err := c.BaseURL.Parse(urlStr)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(method, u.String(), reader)

	if err != nil {
		return nil, err
	}

	req.ContentLength = size

	req.Header.Set("Content-Type", mediaType)

	if c.UserAgent != "" {
		req.Header.Set("User-Agent", c.UserAgent)
	}
	return req, nil
}

func (c *Client) NewRequest(method, urlStr string, body interface{}) (*http.Request, error) {

	if !strings.HasSuffix(c.BaseURL.Path, "/") {
		return nil, fmt.Errorf("BaseURL must have a trailing slash, but %q does not", c.BaseURL)
	}

	u, err := c.BaseURL.Parse(urlStr)
	if err != nil {
		return nil, err
	}

	var buf io.ReadWriter
	if body != nil {
		buf = new(bytes.Buffer)
		enc := json.NewEncoder(buf)
		enc.SetEscapeHTML(false)
		err := enc.Encode(body)
		if err != nil {
			return nil, err
		}
	}

	req, err := http.NewRequest(method, u.String(), buf)
	if err != nil {
		return nil, err
	}

	if body != nil {
		req.Header.Set("Content-Type", "application/json")
	}

	if c.UserAgent != "" {
		req.Header.Set("User-Agent", c.UserAgent)
	}
	return req, nil

}

/*
An Error reports more details on an individual error in an ErrorResponse.
Whatsapp API docs: https://developers.facebook.com/docs/whatsapp/api/reference#response
*/
type Error struct {
	Code    string `json:"code,omitempty"`    // error code
	Title   string `json:"title,omitempty"`   // error message title
	Details string `json:"details,omitempty"` // error details
}

func (e *Error) Error() string {
	return fmt.Sprintf("Err Code: %v; Err Title: %v; Err Details: %v",
		e.Code, e.Title, e.Details)
}

/*
An ErrorResponse reports one or more errors caused by an API request.
*/
type ErrorResponse struct {
	Response *http.Response // HTTP response that caused this error
	Errors   []Error        `json:"errors"` // more detail on individual errors
}

func (r *ErrorResponse) Error() string {
	return fmt.Sprintf("%v %v: %d %+v",
		r.Response.Request.Method, sanitizeURL(r.Response.Request.URL),
		r.Response.StatusCode, r.Errors)
}

// BearerAuthError occurs when HTTP Bearer Auth for a user fails.
type BearerAuthError ErrorResponse

func (r *BearerAuthError) Error() string { return (*ErrorResponse)(r).Error() }

// CheckResponse checks the API response for errors, and returns them if
// present. A response is considered an error if it has a status code outside
// the 200 range or equal to 202 Accepted.
// API error responses are expected to have either no response
// body, or a JSON response body that maps to ErrorResponse. Any other
// response body will be silently ignored.
func CheckResponse(r *http.Response) error {
	if c := r.StatusCode; 200 <= c && c <= 299 {
		return nil
	}
	errorResponse := &ErrorResponse{Response: r}
	data, err := ioutil.ReadAll(r.Body)
	if err == nil && data != nil {
		json.Unmarshal(data, errorResponse)
	}
	switch {
	case r.StatusCode == http.StatusUnauthorized:
		return (*BearerAuthError)(errorResponse)
	default:
		return errorResponse
	}
}

func (c *Client) Do(ctx context.Context, req *http.Request, v interface{}) (*http.Response, error) {
	req = req.WithContext(ctx)

	resp, err := c.client.Do(req)
	if err != nil {
		// If we got an error, and the context has been canceled,
		// the context's error is probably more useful.
		select {
		case <-ctx.Done():
			return nil, ctx.Err()
		default:
		}

		return nil, err
	}

	defer func() {
		if v != nil {
			resp.Body.Close()
		}
	}()

	err = CheckResponse(resp)
	if err != nil {
		return resp, err
	}

	if v != nil {
		if w, ok := v.(io.Writer); ok {
			io.Copy(w, resp.Body)
		} else {
			decErr := json.NewDecoder(resp.Body).Decode(v)
			if decErr == io.EOF {
				decErr = nil // ignore EOF errors caused by empty response body
			}
			if decErr != nil {
				err = decErr
			}
		}
	}

	return resp, err
}

type service struct {
	client *Client
}

type AuthTransport struct {
	Username     string    // Whatsapp username
	Password     string    // Whatsapp password
	Token        string    // Whatsapp token
	ExpiresAfter time.Time // Whatsapp token expiry

	mu sync.RWMutex

	// Transport is the underlying HTTP transport to use when making requests.
	// It will default to http.DefaultTransport if nil.
	Transport http.RoundTripper
}

func (a *AuthTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	// To set extra headers, we must make a copy of the Request so
	// that we don't modify the Request we were given. This is required by the
	// specification of http.RoundTripper.
	//
	// Since we are going to modify only req.Header here, we only need a deep copy
	// of req.Header.

	req2 := new(http.Request)

	*req2 = *req

	// Do not invoke for /v1//users/login API since it has Basic Auth

	// Check req URL
	// Use DefaultTransport if the call is made to /v1/users/login
	// We don't refresh the token in memory on this API call.
	if req2.URL.Path == "/v1/users/login" {
		return http.DefaultTransport.RoundTrip(req)
	}

	req2.Header = make(http.Header, len(req.Header))
	for k, s := range req.Header {
		req2.Header[k] = append([]string(nil), s...)
	}

	// Check if we have token cached
	// If yes, add to request

	currentTime := time.Now()

	// Refresh token an hour before it expires
	if a.Token != "" && currentTime.Add(time.Hour).Before(a.ExpiresAfter) == true {
		// if a.Token != "" {
		req2.Header.Set("Authorization", fmt.Sprintf("Bearer "+a.Token))
		return a.transport().RoundTrip(req2)
	}

	a.mu.Lock()
	defer a.mu.Unlock()

	// If no, then fetch token using user API
	c := NewClient(fmt.Sprintf(req2.URL.Scheme+"://"+req2.Host+"/"), nil)

	u, _, err := c.Users.Login(context.Background(), a.Username, a.Password)

	// TODO: If explicit logout is called, the current token in memory has to be
	// flushed.

	if err != nil {
		fmt.Println(err)
		return http.DefaultTransport.RoundTrip(req)
	}

	// If token is not empty, then logout.
	if a.Token != "" {
		c.Users.Logout(context.Background())
	}

	// Set token in memory
	a.Token = u.User[0].Token
	a.ExpiresAfter, _ = time.Parse(expiresTimeLayout, u.User[0].ExpiresAfter)

	req2.Header.Set("Authorization", fmt.Sprintf("Bearer "+a.Token))

	return a.transport().RoundTrip(req2)

}

func (a *AuthTransport) Client() *http.Client {
	return &http.Client{Transport: a}
}

func (a *AuthTransport) transport() http.RoundTripper {
	if a.Transport != nil {
		return a.Transport
	}
	return http.DefaultTransport
}

func NewClient(baseURL string, httpClient *http.Client) *Client {

	if httpClient == nil {
		httpClient = http.DefaultClient
	}

	base, _ := url.Parse(baseURL)

	c := &Client{client: httpClient, BaseURL: base, UserAgent: userAgent}
	c.common.client = c

	c.Users = (*UserService)(&c.common)
	c.Contacts = (*ContactService)(&c.common)
	c.Messages = (*MessageService)(&c.common)
	c.Media = (*MediaService)(&c.common)

	return c
}

// sanitizeURL redacts the client_secret parameter from the URL which may be
// exposed to the user.
func sanitizeURL(uri *url.URL) *url.URL {
	if uri == nil {
		return nil
	}
	params := uri.Query()
	if len(params.Get("client_secret")) > 0 {
		params.Set("client_secret", "REDACTED")
		uri.RawQuery = params.Encode()
	}
	return uri
}
