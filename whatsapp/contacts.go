package whatsapp

import (
	"context"
	"fmt"
	"net/http"
)

type ContactService service

type Contacts struct {
	Contact []struct {
		Input  string `json:"input,omitempty"`
		Status string `json:"status,omitempty"`
		WAID   string `json:"wa_id,omitempty"`
	} `json:"contacts,omitempty"`
}

type ContactRequest struct {
	Blocking string   `json:"blocking,omitempty"`
	Contacts []string `json:"contacts,omitempty"`
}

func (c *ContactService) Verify(ctx context.Context, cr *ContactRequest) (*Contacts, *http.Response, error) {
	url := fmt.Sprintf("v1/contacts")

	req, err := c.client.NewRequest("POST", url, cr)

	if err != nil {
		return nil, nil, err
	}

	contacts := &Contacts{}

	resp, err := c.client.Do(ctx, req, contacts)

	if err != nil {
		return nil, nil, err
	}

	return contacts, resp, nil
}
