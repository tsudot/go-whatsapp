package whatsapp

import (
	"errors"
)

var ErrMediaSizeExceeded = errors.New("Media size exceeded")
var ErrMediaNotSupported = errors.New("Media not supported")
var ErrMediaContentTypeNotSet = errors.New("Content-Type not set")
var ErrMediaNotReadable = errors.New("Cannot read media")
var ErrMediaContentLength = errors.New("Content-Length not set")
