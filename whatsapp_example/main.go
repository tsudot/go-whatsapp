package main

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/karixtech/go-whatsapp/whatsapp"
	"github.com/davecgh/go-spew/spew"

)

func main() {
	tp := whatsapp.AuthTransport{
		Username: "",
		Password: "",
	}

	// guruNumber := "+917619236333"

	baseURL := ""

	c := whatsapp.NewClient(baseURL, tp.Client())

	// ul2, _, err := c.Users.Login(context.Background(), tp.Username, tp.Password)
	// if err != nil {
	// 	fmt.Println(err)
	// }
	// fmt.Println(ul2)

	bo := []byte(`
			{
				"blocking": "wait",
				"contacts": ["+919986410895"]
			}`)

	cr := &whatsapp.ContactRequest{}

	err := json.Unmarshal(bo, cr)

	if err != nil {
		fmt.Println(err)
	}

	u, _, err := c.Contacts.Verify(context.Background(), cr)

	waID := u.Contact[0].WAID

	b := []byte(`
		{
			"type": "hsm",
			"hsm": { 
				"namespace": "dfb8b7d0_7f19_638a_062f_ee68439568953",
				"element_name": "dispatch2", 
				"language": {
					"policy": "deterministic",
					"code": "en"
				},
				"localizable_params": [ 
					{ "default": "K321232" }, 
					{ "default": "22019" } 
				]
			}  
		}`)

	hsmText := &whatsapp.HSMTextMessage{}

	err = json.Unmarshal(b, &hsmText)

	if err != nil {
		fmt.Println(err)
	}

	hsmText.To = waID

	spew.Dump(hsmText)

	res1B, _ := json.Marshal(hsmText)
	fmt.Println(string(res1B))

	shm, _, err := c.Messages.SendHSM(context.Background(), hsmText)

	if err != nil {
		spew.Dump(err)
	}

	// fmt.Println(shm)
	spew.Dump(shm)

	// cr2 := &whatsapp.ContactRequest{}
	// cr2.Blocking = "wait"
	// cr2.Contacts = append(cr2.Contacts, "+919999406610")
	// u2, _, err := c.Contacts.Verify(context.Background(), cr2)

	// if err != nil {
	// 	fmt.Println(err)
	// }

	// fmt.Println(u2)

}
