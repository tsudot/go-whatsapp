# go-whatsapp #


go-whatsapp is a Go client library for accessing the [Whatsapp API v1][].

go-whatsapp requires Go version 1.9 or greater.



[Whatsapp API v1]: https://developers.facebook.com/docs/whatsapp/api/reference

## Usage ##

```go
import "github.com/karixtech/go-whatsapp/whatsapp" 
```

Construct a new Whatsapp transport with your whatsapp username and password. For example:

```go
tp := whatsapp.AuthTransport{ 
    Username: "",
    Password: "",
}
```

Construct a new Whatsapp client with the URL of your whatsapp-web machine, then use the 
various services on the client to access different parts of the Whatsapp API.

### Contacts API

```go
client := whatsapp.NewClient("https://whatsapp.web-container/", tp.Client())

// make a request to verify contact
b := []byte(`
    {
        "blocking": "wait",
        "contacts": ["+123456789098']
    }`)

cr := &whatsapp.ContactRequest{}

err := json.Unmarshal(bo, cr)

if err != nil {
    fmt.Println(err)
}

contact, _, err := client.Contacts.Verify(context.Background(), cr)

```


### Message API

```go
client := whatsapp.NewClient("https://whatsapp.web-container/", tp.Client())

b := []byte(`
    {
        "to": "12345678901",
        "type": "hsm",
        "hsm": { 
            "namespace": "js338b7d0_7f19_638a_062f_ee6843982353",
            "element_name": "dispatch", 
            "language": {
                "policy": "deterministic",
                "code": "en"
            },
            "localizable_params": [ 
                { "default": "K321232" }, 
                { "default": "22019" } 
            ]
        }
    }`)

hsmText := &whatsapp.HSMTextMessage{}

err = json.Unmarshal(b, &hsmText)

if err != nil {
    fmt.Println(err)
}

shm, _, err := client.Messages.SendHSM(context.Background(), hsmText)
```


### Media API

```go
client := whatsapp.NewClient("https://whatsapp.web-container/", tp.Client())

mediaURL := "https://example.com/image.jpg"

classifiedMedia, _, _ := client.Media.UploadURL(context.Background(), mediaURL)

mediaID := classifiedMedia.MediaData.Media[0].ID

b := []byte(`
    {
        "to": "12345678901",
        "type": "image"
    }`)

mediaMeta := &whatsapp.MediaMeta{}

mediaMeta.ID = mediaID
mediaMeta.Caption = "hello"

imageMessage := &whatsapp.ImageMediaMessage{}
err = json.Unmarshal(b, &imageMessage)

if err != nil {
    fmt.Println(err)
}

imageMessage.Image = mediaMeta

shm, _, err := client.Messages.SendImageMedia(context.Background(), imageMessage)
```


